import { Injectable } from '@angular/core';

@Injectable()
export class NewsService {

  constructor() { }

}
export interface  news {
  content : string;
  title : string;
  id : string;
  img : string;
  link: string;
}
