import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
// tslint:disable-next-line:import-blacklist
import 'rxjs/Rx';

@Injectable()
export class ApiService {

  baseUrl = 'http://localhost:3000/api/';

  constructor(private http: Http) { }

  // get all models
  getAll(model: String): Observable<any> {
    // console.log(this.baseUrl + model);
    return this.http
      .get(this.baseUrl + model)
      .map((response: Response) => {
        return <any>response.json();
      })
      .catch(this.handleError);
  }

  // get all models with filter and skip may be you will use it for pagination
  getAllWithLim(model, lim, skip): Observable<any> {
    // console.log(this.baseUrl + model + '?[filter][limit]=' + lim + '&[filter][skip]=' + skip);
    return this.http
      .get(this.baseUrl + model + '?[filter][limit]=' + lim + '&[filter][skip]=' + skip)
      .map((response: Response) => {
        return <any>response.json();
      })
      .catch(this.handleError);
  }

  // get all models with filter. pass your filter arguments with array of objects
  getAllWithFilter(model, array: object[] ): Observable<any> {
    // console.log(this.baseUrl + model + '?[filter][limit]=' + lim + '&[filter][skip]=' + skip);
    // tslint:disable-next-line:prefer-const
    let first = true;
    let filterUrl = this.baseUrl + model;
    array.forEach(element => {
      if (first === true) {
        filterUrl = filterUrl + '?[' + element['key'] + ']=' + element['value'];
      } else {
        filterUrl = filterUrl + '&[' + element['key'] + ']=' + element['value'];
      }
      first = false;
    });
    return this.http
      .get(filterUrl)
      .map((response: Response) => {
        return <any>response.json();
      })
      .catch(this.handleError);
  }

  // patch all models
  patchAll(model, data, id, token) {
    // console.log(data);
    return this.http
      .patch(this.baseUrl + model + '/' + id + '?access_token=' + token, data)
      .map((response: Response) => {
        return response.json();
      })
      .catch(this.handleError);
  }
  // post all models
  postAll(model , data) {
    // console.log(data);
    return this.http
      .post(this.baseUrl + model, data)
      .map((response: Response) => {
        return response.json();
      })
      .catch(this.handleError);
  }
//////////////test////////////////////////
  postTest( data) {
    // console.log(data);
    return this.http
      .post('http://localhost:3000/api/search/greet', data)
      .map((response: Response) => {
        return response.json();
      })
      .catch(this.handleError);
  }

  search(key: string) {
    return this.http
      .get(this.baseUrl )
      .map((response: Response) => {
        return <any>response.json();
      })
      .catch(this.handleError);
  }
/////////////test//////////////////////////////////
  // lets see what's going on
  private handleError(error: Response) {
    console.log('gorf gorf gorf' + error);
    return Observable.throw(error.statusText);
  }

}
