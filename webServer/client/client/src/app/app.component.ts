import { Component } from '@angular/core';
import {ApiService} from "./services/api.service";
import {news} from "./model/news.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  _news: news;
  constructor(private api: ApiService){
  }

  getAllNews(): void {
    this.api.getAll('news')
      .subscribe(
        resultArray => this._news = resultArray,
        error => console.log('Error :: derash wiha :: ' + error)
      );
  }
  ngOnInit() {
    this.getAllNews();
  }
}
