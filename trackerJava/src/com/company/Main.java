package com.company;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import sun.net.www.http.HttpClient;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
	//declare the global variables
        String title;
        String content;
        String link;
        String img;

        //set the driver
        System.setProperty("webdriver.chrome.driver", "./chromedriver-2");
        WebDriver page = new ChromeDriver();

        //open google news page
        page.get("https://news.google.com/?hl=en-ET&gl=ET&ceid=ET:en");
        List<WebElement> list  = (List<WebElement>) page.findElements(By.className("NiLAwe"));
        for (WebElement element: list){
            title = element.findElement(new By.ByTagName("span")).getText();
            content = element.findElement(new By.ByTagName("p")).getText();
            link = element.findElement(new By.ByClassName("VDXfz")).getAttribute("href");
            String glink =  link;
            img = "";
            if (element.findElements(By.tagName("img")).size()>0){
                img = element.findElement(By.tagName("img")).getAttribute("src");
            }
            //@todo post these datas to web tracker page
            System.out.println(title + " \n " + content + "\nimage \n"+img);


            DefaultHttpClient httpClient = new DefaultHttpClient();

            try {
                HttpPatch request = new HttpPatch("http://localhost:3000/api/news");
                StringEntity params =new StringEntity("{\"content\":\""+content+"\",\"title\":\""+title+"\",\"img\":\""+img+"\",\"link\"" +
                        ":\""+glink+"\"} ");
                request.addHeader("content-type", "application/json");
                request.addHeader("Accept","application/json");
                request.setEntity(params);
                HttpResponse response = httpClient.execute(request);

                // handle response here...
            }catch (Exception ex) {
                // handle exception here
            } finally {
                httpClient.getConnectionManager().shutdown();
            }


        }

        page.get("https://www.telegraph.co.uk/news/");
        List<WebElement> newsList  = (List<WebElement>) page.findElements(By.className("list-of-entities__item "));
        for (WebElement element: newsList){
            title = element.findElement(new By.ByTagName("h3")).findElement(new By.ByTagName("a")).getText();
            content = " ";
//            link = element.findElement(new By.ByClassName("VDXfz")).getAttribute("href");
               String glink =  " ";
            img = "";
            if (element.findElements(By.tagName("img")).size()>0){
                img = element.findElement(By.tagName("img")).getAttribute("src");
            }
            //@todo post these datas to web tracker page
            System.out.println(title + " \n " + content + "\nimage \n"+img);


            DefaultHttpClient httpClient = new DefaultHttpClient();

            try {
                HttpPatch request = new HttpPatch("http://localhost:3000/api/news");
                StringEntity params =new StringEntity("{\"content\":\""+content+"\",\"title\":\""+title+"\",\"img\":\""+img+"\",\"link\"" +
                        ":\""+glink+"\"} ");
                request.addHeader("content-type", "application/json");
                request.addHeader("Accept","application/json");
                request.setEntity(params);
                HttpResponse response = httpClient.execute(request);

                // handle response here...
            }catch (Exception ex) {
                // handle exception here
            } finally {
                httpClient.getConnectionManager().shutdown();
            }


        }


        page.get("https://addisfortune.net/");
        List<WebElement> fortuneList  = (List<WebElement>) page.findElements(By.className("span6"));
        WebElement span6 = fortuneList.get(1);
        List<WebElement> headers = span6.findElements(new By.ByTagName("h3"));
        List<WebElement> rows =span6.findElements(new By.ByClassName("row"));

        for (int i =0; i <headers.size() ; i++){
             title = headers.get(i).getText();
             content = rows.get(i).findElement(By.tagName("p")).getText();
            img = "";
            String glink ="";
            if (rows.get(i).findElements(By.tagName("img")).size()>0){
                img = rows.get(i).findElement(By.tagName("img")).getAttribute("src");
            }
            System.out.println(title + " \n " + content + "\nimage \n"+img);


            DefaultHttpClient httpClient = new DefaultHttpClient();

            try {
                HttpPatch request = new HttpPatch("http://localhost:3000/api/news");
                StringEntity params =new StringEntity("{\"content\":\""+content+"\",\"title\":\""+title+"\",\"img\":\""+img+"\",\"link\"" +
                        ":\""+glink+"\"} ");
                request.addHeader("content-type", "application/json");
                request.addHeader("Accept","application/json");
                request.setEntity(params);
                HttpResponse response = httpClient.execute(request);

                // handle response here...
            }catch (Exception ex) {
                // handle exception here
            } finally {
                httpClient.getConnectionManager().shutdown();
            }

        }

    }
}
